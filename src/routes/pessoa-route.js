const router = require('express-promise-router')();
const pessoaController = require('../controller/pessoa-controller');

router.post('/pessoas', pessoaController.salvarPessoa);
router.get('/pessoas', pessoaController.listarPessoas);
router.get('/pessoas/:id', pessoaController.listarPessoaPorId);
router.put('/pessoas/:id', pessoaController.atualizarPessoa);
router.delete('/pessoas/:id', pessoaController.deletarPessoa);

module.exports = router;