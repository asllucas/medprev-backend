const db = require("../config/database");

exports.salvarPessoa = async (req, res) => {
    const { tipo, nome, razao_social, cpf, cnpj, sexo, data_nascimento, email, telefone, celular, foto, endereco, numero, complemento, bairro, cep, cidade, uf } = req.body;
    const pessoa = await db.query(
        `INSERT INTO pessoas (
            tipo,
            nome,
            razao_social,
            cpf,
            cnpj,
            sexo,
            data_nascimento,
            email,
            telefone,
            celular,
            foto,
            endereco,
            numero,
            complemento,
            bairro,
            cep,
            cidade,
            uf
            ) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)`,
        [tipo, nome, razao_social, cpf, cnpj, sexo, data_nascimento, email, telefone, celular, foto, endereco, numero, complemento, bairro, cep, cidade, uf]
    );

    res.status(201).send({message: "Pessoa salva com sucesso!"});
};

exports.listarPessoas = async (req, res) => {
    const pessoas = await db.query(
        'SELECT * FROM pessoas ORDER BY id_pessoa ASC',
    );
     res.status(200).send(pessoas.rows);
};

exports.listarPessoaPorId = async (req, res) => {
    const pessoaID = parseInt(req.params.id);
    const pessoas = await db.query(
        'SELECT * FROM pessoas WHERE id_pessoa = $1',
        [pessoaID],
    );
    res.status(200).send(pessoas.rows[0]);
};

exports.atualizarPessoa = async (req, res) => {
    const pessoaID = parseInt(req.params.id);
    const { tipo, nome, razao_social, cpf, cnpj, sexo, data_nascimento, email, telefone, celular, foto, endereco, numero, complemento, bairro, cep, cidade, uf } = req.body;

    const response = await db.query(
        `UPDATE pessoas SET 
        tipo = $1,
        nome = $2,
        razao_social = $3,
        cpf = $4,
        cnpj = $5,
        sexo = $6,
        data_nascimento = $7,
        email = $8,
        telefone = $9,
        celular = $10,
        foto = $11,
        endereco = $12,
        numero = $13,
        complemento = $14,
        bairro = $15,
        cep = $16,
        cidade = $17,
        uf = $18
        WHERE 
            id_ = $19`,
        [ tipo, nome, razao_social, cpf, cnpj, sexo, data_nascimento, email, telefone, celular, foto, endereco, numero, complemento, bairro, cep, cidade, uf, pessoaID]
    );

    res.status(200).send({ message: ' Pessoa atualizada com sucesso!' });
};

exports.deletarPessoa = async (req, res) => {
    const pessoaID = parseInt(req.params.id);
    await db.query('DELETE FROM pessoas WHERE id_pessoa = $1', [
        pessoaID,
    ]);

    res.status(200).send({ message: 'Pessoa removida com sucesso!' });
};
